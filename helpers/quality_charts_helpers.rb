module QualityChartsHelpers
  BUGS_DASHBOARD_ID = 576726
  BUGS_OPEN_VS_CLOSED_RATE_CHART_ID = 8789700
  DEFAULT_DATE_RANGE = 730
  DEFAULT_SEVERITIES = %w[severity\ 1 severity\ 2].freeze

  def bugs_open_vs_closed_rate_params(date_range: DEFAULT_DATE_RANGE, stage: nil, group: nil, severities: DEFAULT_SEVERITIES)
    params = {
      dashboard: BUGS_DASHBOARD_ID,
      chart: BUGS_OPEN_VS_CLOSED_RATE_CHART_ID,
      embed: 'v2',
      aggregation: 'monthly',
      daterange: { days: date_range }
    }

    params.merge!(build_chart_filters('stage': stage, 'team_group': group, 'issue_severity': severities))
  end

  ## Accepts hash of sisense filter name to value
  ## Example
  ## { 'stage': 'manage', 'team_group': 'access' }
  def build_chart_filters(config_params)
    config_params.each_with_object(bare_filters) do |(sisense_filter_name, value), filter_hash|
      if value
        filter_hash[:filters] << { name: sisense_filter_name.to_s, value: value }
        filter_hash[:visible] << sisense_filter_name.to_s
      end

      filter_hash
    end
  end

  def bare_filters
    {
      filters: [],
      visible: []
    }
  end
end

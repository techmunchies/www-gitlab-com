---
layout: markdown_page
title: "Being an Ally"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What it means to be an ally
* Take on the struggle as your own
* Stand up, even when you feel uncomfortable
* Transfer the benefits of your privilege to those who lack it
* Acknowledge that while you, too, feel pain, the conversation is not about you

## Concepts & Terms

1. Privilege: an unearned advantage given to some people but not all 
1. Oppression: systemic inequality present throughout society that benefits people with more privilege and is a disadvantage to those with fewer privileges 
1. Ally: a member of a social group that has some privilege, that is working to end oppression and understands their own privilege 
1. Power: The ability to control circumstances or access to resources and/or privileges  
1. Marginalized groups:  a person or group that are treated as unimportant, insignificant or of lower status. In a workplace setting, employees could be treated as invisible, as if they aren't there or their skills or talents are unwelcome or unnecessary

## Tips on being an ally

1. Identifying your power and privilege helps you act as an ally more effectively
1. Follow and support those as much as possible from marginalized groups 
1. When you make a mistake, apologize, correct yourself, and move on
1. Allies spend time educating themselves and use the knowledge to help
1. Allies take the time to think and analyze the situation
1. Allies try to understand Perception vs. Reality
1. Allies don’t stop with their power they also leverage others powers of authority

See our [Ally Resources Page](https://about.gitlab.com/handbook/communication/ally-resources/) for more resources on being an ally. 

## Ally Training 

We held a 50 minute [Live Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning) Ally Training on 2020-01-28. The recording follows along with the [slide deck](https://docs.google.com/presentation/d/18Qyn2mBJu0Loq3x_RT5bL2lnL-3YHvac1sQhmqqZNso/edit?usp=sharing) and [agenda](https://docs.google.com/document/d/1lGPImuahahjDejI5-9cNNCg-NMQJ4GCHO6n0fcntjs8/edit?usp=sharing). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/wwZeFjDc4zE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Being an Ally Knowledge Assessment

Anyone can become certified in GitLab Ally Training. To obtain certification, you will need to complete this [Being an Ally knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLScmd2Z9Vz66yYR6kCKZBZM61aDMFPHlViWCr2ZsGqFbbxIa4w/viewform) and earn at least an 80%. Once the quiz has been passed, you will receive an email with your certification that you can share on your personal LinkedIn or Twitter pages. If you have questions, please reach out to our L&D team at `learning@gitlab.com`.

---
layout: handbook-page-toc
title: "GitLab Technical Certifications"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Technical Certifications

GitLab offers technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

### Overview

GitLab is planning and developing several technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

### Why certification?

#### For employers
Team managers now have a way to confirm their team members possess the skills needed to effectively use GitLab in their daily DevOps tasks. This helps ensure the team will be able to successfully adopt GitLab and make the most of the organization's investment.

#### For individuals

Individual GitLab users who earn certification receive a certification logo they can share on social media to showcase their accomplishment. This helps highlight to colleagues and employers their proficiency in effectively using the GitLab platform.

### Currently available certifications

Here are the certifications being prepared for availability in FY'21.

#### GitLab Certified Associate

Starting in May 2020 GitLab soft-launched the [GitLab Certified Associate](https://about.gitlab.com/services/education/gitlab-certified-associate/) certification. The certification assessments are currently available to GitLab Professional Services customers who purchase the [GitLab with Git Basics](https://about.gitlab.com/services/education/gitlab-basics/) course for their teams. Course participants gain access to the certification assessments immediately after completing their course sessions.

### Planned certifications for FY'21

The following certifications are planned or in development and will be made available through [GitLab Commit 2020](https://about.gitlab.com/events/commit/) in the "GitLab Groundworks" portion of the August 2020 event and/or through Professional Services live training deliveries. Asynchronous self-paced options are planned to become available for individuals to access towards the end of the calendar year.

* GitLab CI/CD Specialist
* GitLab InnerSourcing Specialist
* GitLab Project Management Specialist


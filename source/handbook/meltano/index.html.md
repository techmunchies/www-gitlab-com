---
layout: handbook-page-toc
title: "Meltano Handbook"
---

[Meltano](https://meltano.com) is an [open source](https://gitlab.com/meltano/meltano) platform for
building, running & orchestrating ELT pipelines built out of [Singer](https://www.singer.io/) taps and targets and [dbt](https://www.getdbt.com) models, that you can [run locally or host on any cloud](https://meltano.com/docs/installation.html).

For more information, check out the [Meltano website](https://meltano.com/), [documentation](https://meltano.com/docs/), and [team handbook](https://meltano.com/handbook/).

## Team

- [Douwe Maan](https://about.gitlab.com/company/team/#DouweM) - Project Lead
